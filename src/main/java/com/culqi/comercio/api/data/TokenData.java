package com.culqi.comercio.api.data;

public class TokenData {
    private String token;
    private String private_key;
    private String public_key;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPrivate_key() {
        return private_key;
    }

    public void setPrivate_key(String private_key) {
        this.private_key = private_key;
    }

    public String getPublic_key() {
        return public_key;
    }

    public TokenData setPublic_key(String public_key) {
        this.public_key = public_key;
        return this;
    }
}
