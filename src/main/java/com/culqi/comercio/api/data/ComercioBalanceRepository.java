package com.culqi.comercio.api.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by lvercelli on 1/27/16.
 */
public interface ComercioBalanceRepository extends JpaRepository<ComercioBalanceModel, Long> {

//    @Query("select cb from ComercioBalanceModel cb where cb.fechaPagoEstimado = :date")
//    List<ComercioBalanceModel> listAllToPayAt(@Param("date") Date date);
}