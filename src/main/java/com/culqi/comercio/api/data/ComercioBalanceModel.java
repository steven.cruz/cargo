package com.culqi.comercio.api.data;


import javax.persistence.*;
import java.util.Date;

/**
 * Created by lvercelli on 1/27/16.
 */
@Entity
@Table(name = "comercio_balance")
public class ComercioBalanceModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column(name = "fecha_pago_estimado")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPagoEstimado;

    @Column(name = "fecha_pago_efectivo")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPagoEfectivo;

    @Column(name = "monto_ventas")
    private Long montoVentas;

    @Column(name = "monto_devoluciones")
    private Long montoDevoluciones;

    @Column(name = "monto_deudas")
    private Long montoDeudas;

    @Column(name = "monto_total")
    private Long montoTotal;

    @Column(name = "monto_sub_total")
    private Long montoSubTotal;

    @Column(name = "monto_comision")
    private Long montoComision;

    @Column(name = "monto_impuestos")
    private Long montoImpuestos;

    @Column(name = "comision_fija_monto")
    private Long comisionFijaMonto;

    @Column(name = "comision_fija_impuestos")
    private Long comisionFijaImpuestos;

    @Column(name = "comision_fija_total")
    private Long comisionFijaTotal;

    @Column(name = "comision_plataforma_monto")
    private Long comisionPlataformaMonto;

    @Column(name = "comision_plataforma_impuestos")
    private Long comisionPlataformaImpuestos;

    @Column(name = "comision_plataforma_total")
    private Long comisionPlataformaTotal;

    @Column(name = "monto_comision_total")
    private Long montoComisionTotal;

    @Column(name = "monto_impuesto_total")
    private Long montoImpuestoTotal;

    @Column(name = "monto_cargo_total")
    private Long montoCargoTotal;

    @Column(name = "fecha_operativa")
    private Date fechaOperativa;

    public ComercioBalanceModel() {
        this.montoVentas = 0L;
        this.montoDevoluciones = 0L;
        this.montoDeudas = 0L;
        this.montoTotal = 0L;
        this.montoComision = 0L;
        this.montoImpuestos = 0L;
        this.comisionFijaMonto = 0L;
        this.comisionFijaImpuestos = 0L;
        this.comisionFijaTotal = 0L;
        this.comisionPlataformaMonto = 0L;
        this.comisionPlataformaImpuestos = 0L;
        this.comisionPlataformaTotal = 0L;
        this.montoComisionTotal = 0L;
        this.montoImpuestoTotal = 0L;
    }

    public Long getComisionPlataformaMonto() {
        return comisionPlataformaMonto;
    }

    public ComercioBalanceModel setComisionPlataformaMonto(Long comisionPlataformaMonto) {
        this.comisionPlataformaMonto = comisionPlataformaMonto;
        return this;
    }

    public Long getComisionPlataformaImpuestos() {
        return comisionPlataformaImpuestos;
    }

    public ComercioBalanceModel setComisionPlataformaImpuestos(Long comisionPlataformaImpuestos) {
        this.comisionPlataformaImpuestos = comisionPlataformaImpuestos;
        return this;
    }

    public Long getComisionPlataformaTotal() {
        return comisionPlataformaTotal;
    }

    public ComercioBalanceModel setComisionPlataformaTotal(Long comisionPlataformaTotal) {
        this.comisionPlataformaTotal = comisionPlataformaTotal;
        return this;
    }

    public Long getId() {
        return id;
    }

    public ComercioBalanceModel setId(Long id) {
        this.id = id;
        return this;
    }

    public Date getFechaPagoEstimado() {
        return fechaPagoEstimado;
    }

    public ComercioBalanceModel setFechaPagoEstimado(Date fechaPagoEstimado) {
        this.fechaPagoEstimado = fechaPagoEstimado;
        return this;
    }

    public Date getFechaPagoEfectivo() {
        return fechaPagoEfectivo;
    }

    public ComercioBalanceModel setFechaPagoEfectivo(Date fechaPagoEfectivo) {
        this.fechaPagoEfectivo = fechaPagoEfectivo;
        return this;
    }

    public Long getMontoVentas() {
        return montoVentas;
    }

    public ComercioBalanceModel setMontoVentas(Long montoVentas) {
        this.montoVentas = montoVentas;
        return this;
    }

    public Long getMontoDevoluciones() {
        return montoDevoluciones;
    }

    public ComercioBalanceModel setMontoDevoluciones(Long montoDevoluciones) {
        this.montoDevoluciones = montoDevoluciones;
        return this;
    }

    public Long getMontoDeudas() {
        return montoDeudas;
    }

    public ComercioBalanceModel setMontoDeudas(Long montoDeudas) {
        this.montoDeudas = montoDeudas;
        return this;
    }

    public Long getMontoTotal() {
        return montoTotal;
    }

    public ComercioBalanceModel setMontoTotal(Long montoTotal) {
        this.montoTotal = montoTotal;
        return this;
    }

    public Long getMontoComision() {
        return montoComision;
    }

    public ComercioBalanceModel setMontoComision(Long montoComision) {
        this.montoComision = montoComision;
        return this;
    }

    public Long getMontoImpuestos() {
        return montoImpuestos;
    }

    public ComercioBalanceModel setMontoImpuestos(Long montoImpuestos) {
        this.montoImpuestos = montoImpuestos;
        return this;
    }

    public Long getComisionFijaMonto() {
        return comisionFijaMonto;
    }

    public ComercioBalanceModel setComisionFijaMonto(Long comisionFijaMonto) {
        this.comisionFijaMonto = comisionFijaMonto;
        return this;
    }

    public Long getComisionFijaImpuestos() {
        return comisionFijaImpuestos;
    }

    public ComercioBalanceModel setComisionFijaImpuestos(Long comisionFijaImpuestos) {
        this.comisionFijaImpuestos = comisionFijaImpuestos;
        return this;
    }

    public Long getComisionFijaTotal() {
        return comisionFijaTotal;
    }

    public ComercioBalanceModel setComisionFijaTotal(Long comisionFijaTotal) {
        this.comisionFijaTotal = comisionFijaTotal;
        return this;
    }

    public Long getMontoComisionTotal() {
        return montoComisionTotal;
    }

    public ComercioBalanceModel setMontoComisionTotal(Long montoComisionTotal) {
        this.montoComisionTotal = montoComisionTotal;
        return this;
    }

    public Long getMontoImpuestoTotal() {
        return montoImpuestoTotal;
    }

    public ComercioBalanceModel setMontoImpuestoTotal(Long montoImpuestoTotal) {
        this.montoImpuestoTotal = montoImpuestoTotal;
        return this;
    }

    public Long getMontoCargoTotal() {
        return montoCargoTotal;
    }

    public ComercioBalanceModel setMontoCargoTotal(Long montoCargoTotal) {
        this.montoCargoTotal = montoCargoTotal;
        return this;
    }

    public Date getFechaOperativa() {
        return fechaOperativa;
    }

    public ComercioBalanceModel setFechaOperativa(Date fechaOperativa) {
        this.fechaOperativa = fechaOperativa;
        return this;
    }

    public Long getMontoSubTotal() {
        return montoSubTotal;
    }

    public ComercioBalanceModel setMontoSubTotal(Long montoSubTotal) {
        this.montoSubTotal = montoSubTotal;
        return this;
    }

    public ComercioBalanceModel recalcTotal() {
        setMontoComisionTotal((montoComision != null ? montoComision : 0) + (comisionFijaMonto != null ? comisionFijaMonto : 0) + (comisionPlataformaMonto != null ? comisionPlataformaMonto : 0));
        setMontoImpuestoTotal((montoImpuestos != null ? montoImpuestos : 0) + (comisionFijaImpuestos != null ? comisionFijaImpuestos : 0) + (comisionPlataformaImpuestos != null ? comisionPlataformaImpuestos : 0));
        setMontoCargoTotal(montoComisionTotal + montoImpuestoTotal);
        setMontoSubTotal(
                (montoVentas != null ? montoVentas : 0)
                        - (montoDevoluciones != null ? montoDevoluciones : 0)
                        - (montoCargoTotal != null ? montoCargoTotal : 0)
        );
        return setMontoTotal(
                (montoSubTotal != null ? montoSubTotal : 0)
                        - (montoDeudas != null ? montoDeudas : 0)
        );
    }

    @Override
    public String toString() {
        return "ComercioBalanceModel{" +
                "id=" + id +
                ", fechaPagoEstimado=" + fechaPagoEstimado +
                ", fechaPagoEfectivo=" + fechaPagoEfectivo +
                ", montoVentas=" + montoVentas +
                ", montoDevoluciones=" + montoDevoluciones +
                ", montoDeudas=" + montoDeudas +
                ", montoTotal=" + montoTotal +
                ", montoSubTotal=" + montoSubTotal +
                ", montoComision=" + montoComision +
                ", montoImpuestos=" + montoImpuestos +
                ", comisionFijaMonto=" + comisionFijaMonto +
                ", comisionFijaImpuestos=" + comisionFijaImpuestos +
                ", comisionFijaTotal=" + comisionFijaTotal +
                ", comisionPlataformaMonto=" + comisionPlataformaMonto +
                ", comisionPlataformaImpuestos=" + comisionPlataformaImpuestos +
                ", comisionPlataformaTotal=" + comisionPlataformaTotal +
                ", montoComisionTotal=" + montoComisionTotal +
                ", montoImpuestoTotal=" + montoImpuestoTotal +
                ", montoCargoTotal=" + montoCargoTotal +
                ", fechaOperativa=" + fechaOperativa +
                '}';
    }
}