package com.culqi.comercio.api.controller;

import com.culqi.comercio.api.data.*;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.util.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/cargo")
public class CargoController {

    @Autowired
    private TaskScheduler taskScheduler;

    private Logger logger = LoggerFactory.getLogger(CargoController.class);

//    @Autowired
//    private ComercioBalanceRepository comercioBalanceRepository;

//    @Autowired
//    public CargoController(
//            TaskScheduler taskScheduler
//    ) {
//        this.taskScheduler = taskScheduler;
//    }

    @PostConstruct
    private void scheduleTask() {
        String cronExpr = "1 0 0 * * *";
        logger.info("Programando cierre automático y creación de balances... cron: " + cronExpr);

        try {
            taskScheduler.schedule(this::execution, new CronTrigger(cronExpr));
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

//    @Scheduled(cron = "0 0 * * * *")
    public void execution() {
        logger.info("Iniciando Tarea");
        /**
         * TODO Obtener correctamente la fecha operativa (Período)
         * Por ahora se obtiene del día anterior (porque se ejecuta siempre al comienzo del día)
         */
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DAY_OF_MONTH, -1);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);
    }

    @RequestMapping(value = "/crear", method = RequestMethod.POST)
    public Map<String, Object> create(@RequestBody TokenData tokenData)
    {
        TokenData data2 = new TokenData();
        data2.setToken("11111111111");
        data2.setPrivate_key("11111111111");
        data2.setPublic_key("11111111111");

        TokenData data3 = new TokenData();
        data3.setToken("22222222222");
        data3.setPrivate_key("22222222222");
        data3.setPublic_key("22222222222");

        TokenData data4 = new TokenData();
        data4.setToken("33333333333");
        data4.setPrivate_key("33333333333");
        data4.setPublic_key("33333333333");

        List<TokenData> lista = new ArrayList<>();
        lista.add(data2);
        lista.add(data3);
        lista.add(data4);

        if (true) {
            Map map = new HashMap();
            map.put("rst", 1);
            map.put("data", lista);

            return map;
        }

        logger.info("dentro del metodo crear");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", "application/json");
        headers.set("Authorization", "Bearer " + tokenData.getPrivate_key());

        CargoData cargoData = new CargoData();
        cargoData.setAmount(100L);
        cargoData.setCurrency_code("PEN");
        cargoData.setEmail("test@culqi.com");
        cargoData.setSource_id(tokenData.getToken());

        HttpEntity<CargoData> request = new HttpEntity<CargoData>(cargoData, headers);

        String url = "https://api.culqi.com/v2/charges";

        logger.info("URL: " + url);
        logger.info("Request: " + request.toString());

        try {
            RestTemplate restTemplate = new RestTemplate();
            Map<String, Object> response = restTemplate.postForObject(url, request, Map.class);

            logger.info("response cargo: " + response.toString());

            return response;
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            logger.error("HttpClientErrorException:" + e.getResponseBodyAsString());
            return null;

        } catch (Exception e) {
            logger.error("Exception:" + e.toString());
            return null;
        }

    }

    public void getAllToPayAt(Date date) {

        logger.info("Dentro de getAllToPayAt: " + date);

//        List<ComercioBalanceModel> lista = comercioBalanceRepository.listAllToPayAt(date);
//        for (ComercioBalanceModel x : lista) {
//            logger.info("Datos de llegan de la consulta: " + x.toString());
//        }

        logger.info("terminando recorrido de lista");
        
    }

    public void doCerrarLotes(Date date) {
        logger.info("Realizando cierre automático de todos los lotes abiertos de todos los comercios");
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        logger.info("Fechita: " + calendar.getTime());
    }

    @RequestMapping(value = "/cargopago", method = RequestMethod.POST)
    public Map<String, Object> cargopago(@RequestBody TokenData tokenData)
    {

        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DAY_OF_MONTH, -1);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);

        logger.info("Fecha inicial: " + gc.getTime());
        doCerrarLotes(gc.getTime());

        if (true) {
            return null;
        }

        logger.info("Dentro del metodo doCerrarBalances y GregorianCalendar: " + gc);
        GregorianCalendar payDate = (GregorianCalendar) gc.clone();
        payDate.add(Calendar.DAY_OF_YEAR, 1);

        logger.info("Pasando el input para query: " + payDate.getTime());

        getAllToPayAt(payDate.getTime());

//        select cb.* from comercio_balance cb where cb.fecha_pago_efectivo = :date

        if (true) {
            return null;
        }

        logger.info("dentro del metodo crear");
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", "application/json");
        headers.set("Authorization", "Bearer " + tokenData.getPrivate_key());

        String pedidoIni = "GO";
        int pedidoRandom = (int) (Math.random()*1000 + 1);

        CargoPagoData cargoPago = new CargoPagoData();
        cargoPago.setToken(tokenData.getToken());
        cargoPago.setMoneda("PEN");
        cargoPago.setMonto(101);
        cargoPago.setDescripcion("Prueba v1.2 - Goku");
        cargoPago.setPedido(RandomStringUtils.randomAlphanumeric(22));
        cargoPago.setCodigo_pais("PE");
        cargoPago.setCiudad("Lima");
        cargoPago.setUsuario("70707070");
        cargoPago.setDireccion("Av. Namecu");
        cargoPago.setTelefono("99999999");
        cargoPago.setNombres("Goku");
        cargoPago.setApellidos("Monte Paho");
        cargoPago.setCorreo_electronico("gg@gg.com");

        HttpEntity<CargoPagoData> request = new HttpEntity<CargoPagoData>(cargoPago, headers);

        logger.info("publicKey: " + tokenData.getPublic_key());

        boolean rst = tokenData.getPublic_key().contains("live_");
        String url = rst ? "https://pago.culqi.com/api/v1/cargos" : "https://integ-pago.culqi.com/api/v1/cargos";

        try {

            logger.info("URL: " + url);

            logger.info("Request: " + request.toString());

            RestTemplate restTemplate = new RestTemplate();
            Map<String, Object> response = restTemplate.postForObject(url, request, Map.class);

            logger.info("response cargo: " + response.toString());

            return response;
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            logger.error(e.getMessage(), e);
            logger.error("HttpClientErrorException:" + e.getResponseBodyAsString());
            Map<String, Object> response = new HashMap<>();
            response.put("response", e.getResponseBodyAsString());
            return response;

        } catch (Exception e) {
            logger.error("Exception:" + e.toString());
            Map<String, Object> response = new HashMap<>();
            response.put("response", e.toString());
            return response;
        }

    }
}
